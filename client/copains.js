var nomGroupe = '';
var loggedIn = false;
var update = false;
var updateRate = 5000;
var billetCharge ='';

$(document).ready(function(){


    $('#index form').submit(function(e){
        $.ajax({

            type: 'POST',

            url: 'https://192.168.75.13/api/v2/users/login',

            contentType: 'application/x-www-form-urlencoded',

            data: $(this).serialize(),

            timeout: 3000,

            success: function(data){
                loggedIn = true;
                window.location.hash='#groupes';
            },

            error: function(){
                alert('Impossible de se connecter.')
            }

        }).done(function (data, textStatus, xhr) {
            let auth = xhr.getResponseHeader('Authorization');
            Cookies.set("Authorization", auth, {expires: 1});
        });
        e.preventDefault();
        return false;
    });

    $('#deco form').submit(function(e){

        $.ajax({
            type: 'POST',

            url: 'https://192.168.75.13/api/v2/users/logout',

            contentType: 'application/x-www-form-urlencoded',

            timeout: 3000,

            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
            },

            success: function(data){
                Cookies.set("Authorization", "", {expires: 1});
                window.location.hash= '#index';
                loggedIn = false;
                console.log("Deconnexion");
                window.location.hash = '#index';
                switchMenu();
            },

            error: function(){
                alert('Impossible de se déconnecter.')
            }

        });
        e.preventDefault();
        return false;
    });

    $('#groupes form').submit(function(e){
        $.ajax({

            type: 'POST',

            url: 'https://192.168.75.13/api/v2/groupes',

            contentType: 'application/x-www-form-urlencoded',

            data: $(this).serialize(),

            timeout: 3000,

            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
            },

            success: function(data){
                window.location.hash='#groupe';
                document.location.href="#groupes";
            },

            error: function(){
                alert('Impossible de créer un groupe.');
            }

        });
        e.preventDefault();
        return false;
    });

    $('#createBillet').submit(function(e){
        $.ajax({
            type: 'POST',

            url: 'https://192.168.75.13/api/v2/groupes/'+nomGroupe+'/billets',

            data: $(this).serialize(),

            contentType: 'application/x-www-form-urlencoded',

            timeout: 3000,

            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
            },

            success: function(data){
                alert("Creation de billet dans le groupe "+nomGroupe);
                updateBillets();
            },

            error: function(){
                alert('Impossible de créer un billet.');
            }
        });
        e.preventDefault();
        return false;
    });

    $('#deleteGroupe').submit(function(e){
        $.ajax({
            type: 'DELETE',

            url: 'https://192.168.75.13/api/v2/groupes/'+nomGroupe,

            timeout: 3000,

            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
            },

            success: function(data){
                alert("Suppression du groupe "+nomGroupe);
                nomGroupe = '';
                window.location.hash = '#groupes';
            },

            error: function(){
                alert('Impossible de supprimer ce groupe.');
            }
        });
        e.preventDefault();
        return false;
    });

    $('#deleteBillet').submit(function(e){
        $.ajax({
            type: 'DELETE',

            url: 'https://192.168.75.13/api/v2/groupes/'+nomGroupe+'/billets/'+billetCharge,

            timeout: 3000,

            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
            },

            success: function(){
                alert("Suppression du billet "+billetCharge);
                billetCharge = '';
                window.location.href = '#groupe';
            },

            error: function(){
                alert('Impossible de supprimer ce billet.');
            }
        });
        e.preventDefault();
        return false;
    });

    $('#addComment').submit(function(e){
        $.ajax({
            type: 'POST',

            url: 'https://192.168.75.13/api/v2/groupes/'+nomGroupe+'/billets/'+billetCharge+'/commentaires',

            data: $(this).serialize(),

            contentType: 'application/x-www-form-urlencoded',

            timeout: 3000,

            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
            },

            success: function(data){
                console.log("Creation de commentaire dans le billet "+billetCharge);
                updateComs(billetCharge);
            },

            error: function(){
                alert('Impossible de créer un commentaire.');
            }
        });
        e.preventDefault();
        return false;
    });

    $( window ).on( 'hashchange', function( e ) {
        if (window.location.hash != '#index' && !loggedIn) {
            window.location.hash = '#index';
            alert('Vous devez être connecté.');
        } else if (window.location.hash == '#groupes') {
            $.ajax({

                type: 'GET',

                url: 'https://192.168.75.13/api/v2/groupes',

                dataType: "json",

                timeout: 3000,

                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
                },

                success : function(data){
                    $('#groupesList').html('');
                    let template = $('#templateListeGroupe').html();
                    data.forEach(function(groupe){
                        let groupeName = groupe.split("groupes/")[1];
                        let rendered = Mustache.render(template, {groupe:groupeName});
                        var $rendered = $(rendered);
                        $rendered.find('a').click(chargerGroupe);
                        $('#groupesList').append($rendered);
                    });
                    switchMenu();
                },

                error : function(data){
                    console.log('erreur de chargement des groupes');
                }
            });
        } else if (window.location.hash == '#groupe') {
            console.log("affichage du groupe");
            if (nomGroupe) {
                switchMenu();
            } else {
                alert('Vous devez choisir un groupe avant.');
            }
        } else if (window.location.hash =='#users') {

            $.ajax({
                type: 'GET',

                url: 'https://192.168.75.13/api/v2/users',

                dataType: "json",

                timeout: 3000,

                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
                },

                success: function(data) {
                    $('#usersList').html('');
                    data.forEach(function(user){
                        let userId = user.split('users/')[1];
                        let template = $("#templateListeUsers").html();
                        let rendered = Mustache.render(template, {user:userId});
                        $('#usersList').append(rendered);
                    });
                    switchMenu();
                }
            });
        } else if (window.location.hash =='#deco' && loggedIn) {
            switchMenu();
    }
    } );

    console.log("chargement initial");
});

function switchMenu() {
    $('.active').addClass('inactive').removeClass('active');
    $(window.location.hash).addClass('active').removeClass('inactive');
}

function chargerGroupe() {
    var groupId = $(this).attr("dataGroupe");
    nomGroupe = groupId;
    console.log("nom groupe chargé" + nomGroupe);
    $.ajax({

        type: 'GET',

        url: 'https://192.168.75.13/api/v2/groupes/'+nomGroupe,

        dataType: "json",

        timeout: 3000,

        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
        },

        success : function(data){
            console.log(data);
            $('#groupeInfos').html('');
            let template = $('#templateGroupe').html();
            let groupId = data.nom;
            let userId = data.auteur.split("users/")[1];
            let rendered = Mustache.render(template, {id:nomGroupe, user:userId});
            var $rendered = $(rendered);
            $('#groupeInfos').append($rendered);
            window.location.hash = "#groupe";
            switchMenu();
        },

        error : function(data){
            alert('Erreur de chargement du groupe, vous netes peut être pas son propriétaire !');
        }
    });

   updateBillets();
}

function chargerBillet(){
    var billetId = $(this).attr("databillet");
    console.log("id billet chargé " + billetId);
    $.ajax({

        type: 'GET',

        url: 'https://192.168.75.13/api/v2/groupes/'+nomGroupe+'/billets/'+billetId,

        dataType: "json",

        timeout: 3000,

        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
        },

        success : function(data){
            $('#billetInfos').html('');
            let template = $('#templateBillet').html();
            let billetTitre = data.titre;
            let billetContenu = data.contenu;
            let billetAuteur = data.auteur.split('users/')[1];
            let rendered = Mustache.render(template, {titre:billetTitre, contenu:billetContenu, auteur:billetAuteur, billetIde:billetId});
            var $rendered = $(rendered);
            $rendered.find('form').submit(updateBillet);
            $('#billetInfos').append($rendered);
            window.location.hash = "#billet";
            billetCharge = billetId;
            switchMenu();
        },

        error : function(data){
            alert('Erreur de chargement du billet !');
        }
    });
    updateComs(billetId);
}

function updateComs(billet){
    $.ajax({

        type: 'GET',

        url: 'https://192.168.75.13/api/v2/groupes/'+nomGroupe+'/billets/'+billet+'/commentaires',

        dataType: "json",

        timeout: 3000,

        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
        },

        success : function(data){
            console.log(data);
            $('#commentList').html('');
            let template = $('#templateListeCommentaires').html();
            data.forEach(function(comment){
                console.log(comment);
                chargerCom(comment,template)
            });
        },

        error : function(data){
            console.log('erreur de chargement des commentaires');
        }
    });
}

function chargerCom(com,template){
    $.ajax({

        type: 'GET',

        url: com,

        dataType: "json",

        timeout: 3000,

        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
        },

        success : function(data){
            console.log(data);
            let auteur = data.auteur.split("users/")[1];
            let texte = data.texte;
            let rendered = Mustache.render(template, {auteur:auteur, texte:texte});
            var $rendered = $(rendered);
            $('#commentList').append($rendered);
        },

        error : function(data){
            console.log('erreur de chargement du commentaire');
        }
    });
}

function updateBillet() {
    var billetId = $(this).attr("databillet");
    console.log(billetId);
    $.ajax({
        type: 'PUT',

        url: 'https://192.168.75.13/api/v2/groupes/'+nomGroupe+'/billets/'+billetId,

        data: $(this).serialize(),

        contentType: 'application/x-www-form-urlencoded',

        timeout: 3000,

        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
        },

        success: function(data){
            alert("Modification du billet "+billetId);
        },

        error: function(){
            alert('Impossible de modifier le billet.');
        }
    });
    return false;
}

function updateBillets() {
    $.ajax({

        type: 'GET',

        url: 'https://192.168.75.13/api/v2/groupes/'+nomGroupe+'/billets',

        dataType: "json",

        timeout: 3000,

        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", Cookies.get("Authorization"));
        },

        success : function(data){
            $('#bltList').html('');
            let template = $('#templateListeBillets').html();
            console.log(data);
            data.forEach(function(billet){
                let billetId = billet.split("billets/")[1];
                console.log(billet);
                let rendered = Mustache.render(template, {billet:billetId, groupe:nomGroupe});
                var $rendered = $(rendered);
                $rendered.find('a').click(chargerBillet);
                $('#bltList').append($rendered);
            });
        },

        error : function(data){
            console.log('erreur de chargement des billets');
        }
    });
}

