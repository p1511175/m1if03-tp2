# M1IF03-TP2 - Programmation Java côté serveur

### 1.1 Gestion des billets et des commentaires
- A sa connexion, j'ai décidé d'orienter l'utilisateur vers un home.jsp où sont affichés les titres cliquables des billets déjà créés.
- Là il peut cliquer sur "Saisir un nouveau billet", ce qui l'orientra vers :
    saisirbillet.jsp dont l'action est gérée par la servlet SaisieBillet.
- Un billet est caractérisé par son titre, son contenu, son auteur et le timestamp de sa création.
- Chaque billet a une liste de commentaires, l'ajout d'un commentaire est géré par la servlet AddComment.
- Un commentaire est caractérisé par son titre, son contenu, son auteur et le timestamp.
- Si l'utilisateur clique sur un des billets pour le consulter la requête sera traitée par la servlet AfficherBillet qui transmettra le Billet à afficher à billet.jsp
#### Gestion d'erreurs : 
- Pseudo necéssaire pour se connecter
- Pas de titre ou de contenu vide pour un billet
- Pas 2 billets avec le même titre
- Pas de commentaire vide
##
### 1.2 Déconnexion
- Ajout de la possibilité de se déconnecter depuis n'importe quel page.
- La ressource home.jsp (équivalent de billet.jsp dans le squelette anciennement fourni) n'est pas accessible si l'utilisateur n'est pas connecté (redirection vers index.html dans la servlet Init si l'attribut de session "pseudo" n'est pas défini).
##
### 1.3 Ajout d'un menu
- Vu que je n'avais pas bien compris "1.1.4 : Mettez en place un mode d'appel de la page billet.jsp pour que l'utilisateur puisse choisir quel billet afficher"
J'ai automatiquement fait dès le début un menu home.jsp qui affiche le titre et le créateur de chaque billet disponible et permet d'être orienter vers la page de création d'un nouveau billet.
- Cliquer sur le titre d'un billet vous donnera accès à une ressource billet/titredubillet affichant ses détails.
- Il faut bloquer également l'accès à /billet/* , à /saisircomment/* et à /saisirbillet pour tout utilisateur non connecté, et ce en les redirigeant vers /init qui les redirigera vers index.html, ou bien s'ils sont connectés et qu'ils essaient d'accéder à saisircomment/* où à un billet qui n'existe pas ça les redirige vers home.jsp (init).
##
### 1.4 Ajout d'un groupe
- L'utilisateur à sa connexion s'assigne un groupe, si celui-ci n'a pas encore été créé il est ajouté à la liste globale GestionGroupes de la session.
- Dans la jsp home.jsp, j'ai décidé de garder l'affichage de tous les billets en plus de la possibilité de choisir dans le menu de groupe un groupe duquel on peut consulter tous les billets (les billets écrits par les gens appartenant à ce groupe).
- Le traitement se fait via la servlet AfficherGroupe et la jsp groupe.jsp


# FIN TP2

##

# M1IF03-TP3 - Design patterns côté serveur


### 1.1 Pattern contexte
- Ayant déjà fait du J2E avant cette UE j'ai directement pensé à utiliser le ServletContext dès le TP2.
- Néanmoins, je ne vois pas l'utilité d'utiliser une HashMap dans mon implémentation à part nous apprendre à les utiliser, ce qui a déjà été fait dans des UEs précédentes.
##
### 1.2 Pattern MVC
- Avec le peu d'expérience que j'ai en design pattern et en J2E j'ai dès le début du TP2 opté pour un MVC avec contrôle d'accès aux vues pour les utilisateurs en général, et qui vérifie que l'utilisateur a bien défini son pseudo avant d'avoir accès aux étapes et aux fonctionnalités postérieures.
- L'utilisateur peut avec ce qui a été implémenté jusqu'ici choisir un groupe déjà existant après avoir défini son pseudo, ou en créer un nouveau. 
- Il a accès à une liste des groupes déjà créés avec une brève description de chacun après la connexion.
- J'ai décidé de laisser la liste totale des billets disponible pour tous les utilisateurs connectés, avec la possibiltié d'aller consulter les billets spéifiques à un groupe en particulier.
##
### 1.3 Pattern chaîne de responsabilité
- Les deux Filter sont implémentés via les servlets LoginFilter pour 1.3.1 et AuthFilter pour 1.3.2.
##
### 2.1 Gestion du cache (Utilisation des en-têtes HTTP)
- Pour cette partie j'ai juste implémenté la méthode getLastModified(request) dans la Servlet Init ainsi que dans GestionBillets bien évidemment.
- Cela a posé un problème pour la redirection de choix d'un groupe existant via Init... La solution que je me suis proposé à cette problématique après moultes essais est de "tricher" en utilisant une seconde Servlet capable de rediriger vers home.jsp et qui est 'RedirectHome'.

##

### Déploiement TP2 & TP3 : 
- http://192.168.75.59:8080/v1/
- https://192.168.75.59/api/v1 : Pour une raison qui m'échappe, malgré la redirection qui s'effectue proprement, ce déploiement n'est pas fonctionnel 
# FIN TP3
##

# M1IF03-TP5 - Programmation côté client (requêtage asynchrone)

### Description de ce qui a été fait :

REQUETES IMPLEMENTEES (dans l'ordre d'apparition dans le script copains.js) : 
- Login (POST)
- Logout (POST)
- Création de groupe (POST)
- Création de billet (POST)
- Suppression de groupe (DELETE)
- Suppression de billet (DELETE)
- Création d'un commentaire (POST)
- Récupération des groupes (GET)
- Récupération de la liste des utilisateurs (GET)
- Récupération des infos d'un groupe (et rejonction d'un groupe dont on est propriétaire uniquement) (GET)
- Récupération des infos d'un billet (GET)
- Récupération des commentaires d'un billet (GET)
- Récupération des infos d'un commentaire (GET)
- Modification d'un billet (PUT)
- Récupération des billets d'un groupe (GET)

# FIN TP5
##

# M1IF03-TP7 - Optimisation d'une Single-Page Application

### Description de ce qui a été fait :

#### Analyse du déploiement sur Tomcat :

- Temps de chargement de la page HTML initiale : 4 ms
- Temps d'affichage de l'app shell : 420 ms
- Temps d'affichage du chemin critique de rendu (CRP) : 427 ms

#### Analyse du déploiement sur nginx en tant que fichiers statiques :

- Temps de chargement de la page HTML initiale : 1 ms (Amélioration : 75%)
- Temps d'affichage de l'app shell : 371 ms (Amélioration : 12%)
- Temps d'affichage du chemin critique de rendu (CRP) : 380 ms (Amélioration : 11%)

#### Optimisation de l'application :

##### Audit initial :
- Performance : 100%
- Accessibilité : 46%
- Best Practices : 86%
- SEO : 60%

##### Upgrade de jQuery 3.3.1 à 3.4.1 :
- Performance : 100%
- Accessibilité : 46%
- Best Practices : 93%
- SEO : 75%

##### Rajout de balises meta dans le html
- Performance : 100%
- Accessibilité : 58%
- Best Practices : 93%
- SEO : 100%

##### Amélioration des font-size / weight
- Performance : 100%
- Accessibilité : 64%
- Best Practices : 93%
- SEO : 100%

##### Ajout de labels
- Performance : 100%
- Accessibilité : 87%
- Best Practices : 93%
- SEO : 100%

##### Ajustement Menu
- Performance : 100%
- Accessibilité : 100%
- Best Practices : 93%
- SEO : 100%

#### Différence App shell et CRP :
- L'app shell constitue les éléments html qui sont en dur et qui sont visualisables via ctrl+u.
- Le CRP consiste en les éléments générés par les scripts dans notre cas en utilisant les templates.
##
### Déploiement TP5 & TP7 : 
- https://192.168.75.59/
- https://192.168.75.59/client/


# FIN TP7
##

## Najib EL KHADIR - P1511175

