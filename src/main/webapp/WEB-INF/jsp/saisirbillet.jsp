<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html lang="fr">

    <head>
        <title>Saisir un nouveau billet</title>
    </head>

    <body>
        <c:set var="pseudo" value="${sessionScope.pseudo}" scope="page"/>

        <h1>Saisie d'un nouveau billet</h1>
            <form method="post" action="saisirbillet">
                <p>
                    <label for="titre">Titre :</label>
                    <input id="titre" type="text" name="titre">
                </p>
                <p>
                    <label for="contenu">Contenu :</label>
                    <textarea id="contenu" name="contenu"></textarea>
                </p>

                <p><input type="submit" value="Ajouter"></p>
            </form>

            <hr>
            <form method="post" action="deco">
                <p><input type="submit" value="Se déconnecter"></p>
            </form>
    </body>
</html>