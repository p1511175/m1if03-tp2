<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html lang="fr">

    <c:set var="billet" value="${requestScope.billet}" scope="page"/>
    <head>
        <title><c:out value="${billet.titre}"/></title>
    </head>

    <body>
        <h1><c:out value="${billet.titre}"/></h1>

        <div class="auteur">
            <p>Auteur :
                <c:out value="${billet.auteur}"/>
            </p>
            <p>Groupe :
                <c:out value="${billet.groupe}"/>
            </p>
        </div>

        <div class="contenu">
            <p> <c:out value="${billet.contenu}"/></p>
        </div>

        <hr>
        <div class="commentaires">
            <c:set var="coms" value="${billet.commentaires}" scope="page"/>
            <c:choose>
                <c:when test="${empty coms}">
                    <p>Aucun commentaire pour l'instant.</p>
                </c:when>
                <c:otherwise>
                    <p>Commentaires : </p>
                    <c:forEach var="com" items="${coms}">
                        <p>
                            <c:out value="${com.auteur}"/>
                            at
                            <c:out value="${com.time}"/>
                            :
                            <c:out value="${com.contenu}"/>
                        </p>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </div>
        <form method="post" action="../saisircomment/${billet.titre}">
            <p>
                <label for="commentaire"> Nouveau commentaire :</label>
                <input id="commentaire" type="text" name="newCom">
                <input type="submit" value="Poster un commentaire">
            </p>
        </form>

        <hr>
        <form method="post" action="../deco">
            <p><input type="submit" value="Se déconnecter"></p>
        </form>
    </body>

</html>
