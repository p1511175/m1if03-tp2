<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Création d'un nouveau group</title>
</head>

<body>
    <c:set var="pseudo" value="${sessionScope.pseudo}" scope="page"/>

    <h1>Création d'un nouveau groupe</h1>
    <h3>Bonjour <c:out value="${pseudo}"/>, veuillez renseigner les champs suivants avec soin</h3>
    <form method="post" action="creergroupe">
        <p>
            <label for="nom">Nom :</label>
            <input id="nom" type="text" name="nom">
        </p>
        <p>
            <label for="description">Description :</label>
            <textarea id="description" name="description"></textarea>
        </p>

        <p><input type="submit" value="Creer"></p>
    </form>

    <hr>
    <form method="post" action="deco">
        <p><input type="submit" value="Se déconnecter"></p>
    </form>
    </body>
</html>