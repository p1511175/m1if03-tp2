<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Choix du groupe</title>
</head>

<body>
    <h1>Choisissez votre groupe :</h1>

    <div class="groupes">
        <h2> Les groupes existants rejoignables:</h2>
        <div class="groupesExistants">
            <c:choose>
                <c:when test="${empty groupes}">
                    <p>Aucun groupe créé.</p>
                </c:when>
                <c:otherwise>
                    <ul>
                        <c:forEach var="groupe" items="${groupes}">
                            <li>
                                <p>
                                    <a href="choisirgroupe/${groupe.nom}">
                                        <c:out value="${groupe.nom}"/>
                                    </a> créé par <c:out value="${groupe.proprio}"/>
                                </p>
                                <p>Membres :</p>
                                <div>
                                    <ul>
                                        <c:forEach var="membre" items="${groupe.membres}">
                                        <li>
                                            <c:out value="${membre}"/>
                                        </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <p> Description : <c:out value="${groupe.description}"/></p>
                            </li>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
        </div>

        <hr>

        <div class="creerGroupe">
            <h2>
                <a href="creergroupe">
                Creer un groupe
                </a>
            </h2>
        </div>

    </div>


    <hr>
    <form method="post" action="deco">
        <p><input type="submit" value="Se déconnecter"></p>
    </form>
</body>

</html>
