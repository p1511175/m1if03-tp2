<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<!doctype html>
<html>

    <head>
        <title>Affichage des billets existants</title>
        <% response.addIntHeader("Refresh", 5); %>
    </head>

    <body>

        <c:set var="pseudo" value="${sessionScope.pseudo}" scope="page"/>
        <c:set var="groupe" value="${sessionScope.groupe}" scope="page"/>
        <h1>Bienvenue sur notre site </h1>
            <h3><c:out value="${pseudo}"/>, du groupe <c:out value="${groupe}"/> </h3>


        <h2> Les groupes existants :</h2>
        <div class="groupes">
            <c:choose>
                <c:when test="${empty groupes}">
                    <p>Aucun groupe créé.</p>
                </c:when>
                <c:otherwise>
                    <ul>
                        <c:forEach var="groupe" items="${groupes}">
                            <li>
                                <a href="groupe/${groupe.nom}">
                                    <c:out value="${groupe.nom}"/>
                                </a>
                            </li>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
        </div>


        <h2> Tous les billets disponibles :</h2>
        <div class="billets">
            <c:choose>
                <c:when test="${empty billets}">
                    <p>Aucun billet créé.</p>
                </c:when>
                <c:otherwise>
                    <ul>
                        <c:forEach var="billet" items="${billets}">
                            <li>
                                <a href="billet/${billet.titre}">
                                    <c:out value="${billet.titre}"/>
                                </a>
                                by
                                <c:out value="${billet.auteur}"/>
                                at
                                <c:out value="${billet.time}"/>
                            </li>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
        </div>

        <hr>
        <p><a href="saisirbillet">Saisir un nouveau billet</a></p>

        <hr>
        <form method="post" action="deco">
            <p><input type="submit" value="Se déconnecter"></p>
        </form>

    </body>
</html>
