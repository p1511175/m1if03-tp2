<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html lang="fr">

<c:set var="groupe" value="${requestScope.groupe}" scope="page"/>
<head>
    <title><c:out value="Groupe ${groupe}"/></title>
</head>

<body>
<h1><c:out value="${groupe}"/></h1>

<div class="groupe">
    <p>Groupe :
        <c:out value="${groupe}"/>
    </p>
</div>

<div class="billetsGroupe">
    <c:choose>
        <c:when test="${empty billets}">
            <p>Aucun billet créé.</p>
        </c:when>
        <c:otherwise>
            <ul>
                <c:forEach var="billet" items="${billets}">
                    <c:set var="grp" value="${billet.groupe}"/>
                    <c:choose>
                        <c:when test="${grp eq groupe}">
                            <li>
                                <a href="../billet/${billet.titre}">
                                    <c:out value="${billet.titre}"/>
                                </a>
                                by
                                <c:out value="${billet.auteur}"/>
                                at
                                <c:out value="${billet.time}"/>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <!-- do nothing -->
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
        </c:otherwise>
    </c:choose>
</div>


<hr>
<form method="post" action="../deco">
    <p><input type="submit" value="Se déconnecter"></p>
</form>
</body>

</html>
