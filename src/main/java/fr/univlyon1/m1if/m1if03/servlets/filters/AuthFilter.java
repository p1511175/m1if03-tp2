package fr.univlyon1.m1if.m1if03.servlets.filters;

import fr.univlyon1.m1if.m1if03.classes.billet.Billet;
import fr.univlyon1.m1if.m1if03.classes.billet.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.groupe.GestionGroupes;
import fr.univlyon1.m1if.m1if03.classes.groupe.Groupe;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "AuthFilter", urlPatterns = {"/saisirbillet", "/billet/*"})
public class AuthFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        GestionGroupes groupes = (GestionGroupes) req.getServletContext().getAttribute("groupes");
        String servletPath = req.getServletPath();

        String pseudo = (String) req.getSession().getAttribute("pseudo");
        String groupe = (String) req.getSession().getAttribute("groupe");

        if (servletPath.equals("/saisirbillet") && !groupes.getGroupeByNom(groupe).isMembre(pseudo)) {
            res.setStatus(302);
            res.sendRedirect(req.getContextPath() + "/init");
        } else {
            Groupe toVerify = null;
            boolean checked = false;

            if(!servletPath.equals("/saisirbillet")) {
                Billet billet = ((GestionBillets) req.getServletContext().getAttribute("billets")).getBilletByTitre(req.getPathInfo().substring(1));
                toVerify = groupes.getGroupeByNom(billet.getGroupe());
                checked = true;
            } else if("POST".equalsIgnoreCase(req.getMethod())) {
                toVerify = groupes.getGroupeByNom(req.getSession().getAttribute("groupe").toString());
                checked = true;
            } else {
                chain.doFilter(request, response);
            }

            if(checked) {
                if(groupes.getGroupeByNom(groupe).isMembre(pseudo)) {
                    chain.doFilter(request, response);
                } else {
                    res.setStatus(302);
                    res.sendRedirect(req.getContextPath() + "/init");
                }
            }
        }
    }
}
