package fr.univlyon1.m1if.m1if03.servlets.actions;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class RedirectHome extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("pseudo")!=null) {
            if(session.getAttribute("groupe")!=null) {
                request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);
            }
            request.getRequestDispatcher("/WEB-INF/jsp/choixgroupe.jsp").forward(request, response);
        } else {
            response.sendRedirect("index.html");
        }
    }

}
