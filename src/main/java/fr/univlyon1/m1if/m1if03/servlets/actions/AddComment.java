package fr.univlyon1.m1if.m1if03.servlets.actions;

import fr.univlyon1.m1if.m1if03.classes.billet.Billet;
import fr.univlyon1.m1if.m1if03.classes.billet.Comment;
import fr.univlyon1.m1if.m1if03.classes.billet.GestionBillets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class AddComment extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String titre = request.getPathInfo().substring(1);
        Billet billet = ((GestionBillets) getServletContext().getAttribute("billets"))
                .getBilletByTitre(request.getPathInfo().substring(1));

        if (billet != null) {
            String contenu = request.getParameter("newCom");
            if (contenu != null && !contenu.isEmpty()) {
                HttpSession session = request.getSession(true);
                billet.addCom(
                        new Comment(
                                (session.getAttribute("pseudo")).toString(), contenu
                        )
                );
                request.setAttribute("billet", billet);
                response.sendRedirect("../billet/" + titre);
            } else {
                response.sendRedirect("../billet/" + titre);
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/init");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("pseudo")==null) {
            response.sendRedirect(request.getContextPath() + "/init");
        } else {
            String titre = request.getPathInfo().substring(1);
            Billet billet = ((GestionBillets) getServletContext().getAttribute("billets"))
                    .getBilletByTitre(request.getPathInfo().substring(1));
            if (billet != null) {
                response.sendRedirect(request.getContextPath() + "/billet/" + titre);
            } else {
                request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);
            }
        }
    }
}
