package fr.univlyon1.m1if.m1if03.servlets.actions;

import fr.univlyon1.m1if.m1if03.classes.billet.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.groupe.GestionGroupes;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Init extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext app = config.getServletContext();
        app.setAttribute("billets", new GestionBillets());
        app.setAttribute("groupes", new GestionGroupes());
        super.init(config);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pseudo = request.getParameter("pseudo");
        if(pseudo != null && !pseudo.equals("")) {
            HttpSession session = request.getSession(true);
            session.setAttribute("pseudo", pseudo);
            response.sendRedirect(request.getContextPath() + "/init");
        } else {
            response.sendRedirect("index.html");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("pseudo")!=null) {
            if(session.getAttribute("groupe")!=null) {
                request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);
            }
            request.getRequestDispatcher("/WEB-INF/jsp/choixgroupe.jsp").forward(request, response);
        } else {
            response.sendRedirect("index.html");
        }
    }


    @Override
    protected long getLastModified(HttpServletRequest req) {
        GestionBillets billets = (GestionBillets) req.getServletContext().getAttribute("billets");
        ZonedDateTime zonedDateTime = ZonedDateTime.of(billets.getLastModified(), ZoneId.of("GMT"));
        return zonedDateTime.toInstant().toEpochMilli();
    }


}
