package fr.univlyon1.m1if.m1if03.servlets.actions;

import fr.univlyon1.m1if.m1if03.classes.groupe.GestionGroupes;
import fr.univlyon1.m1if.m1if03.classes.groupe.Groupe;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CreerGroupe extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String nom = request.getParameter("nom");
        String description = request.getParameter("description");

        if (nom != null && !nom.isEmpty() && description != null && !description.isEmpty()) {
            ServletContext app = getServletContext();
            GestionGroupes groupes = (GestionGroupes) app.getAttribute("groupes");

            if (groupes.getGroupeByNom(nom)==null) {
                Groupe groupe = new Groupe(
                        nom,
                        description,
                        request.getSession().getAttribute("pseudo").toString()
                );
                groupes.add(groupe);
                HttpSession session = request.getSession(true);
                session.setAttribute("groupe", groupe.getNom());
                response.sendRedirect(request.getContextPath() + "/home");
            } else {
                response.sendRedirect(request.getContextPath() + "/creergroupe");
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/creergroupe");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("pseudo")!=null) {
            request.getRequestDispatcher("/WEB-INF/jsp/creergroupe.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/init");
        }
    }
}
