package fr.univlyon1.m1if.m1if03.servlets.actions;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AfficherGroupe extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("pseudo")==null) {
            response.sendRedirect(request.getContextPath() + "/init");
        }

        else {
            String groupe = (String) (request.getPathInfo().substring(1));
            if (groupe != null) {
                request.setAttribute("groupe", groupe);
                request.getRequestDispatcher("/WEB-INF/jsp/groupe.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/init");
            }
        }
    }
}