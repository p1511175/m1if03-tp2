package fr.univlyon1.m1if.m1if03.servlets.actions;

import fr.univlyon1.m1if.m1if03.classes.groupe.GestionGroupes;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChoisirGroupe extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nomGroupe =  request.getPathInfo().substring(1);
        HttpSession session = request.getSession(true);
        if(session.getAttribute("pseudo")==null) {
            response.sendRedirect(request.getContextPath() + "/init");
        } else {
            String pseudo = session.getAttribute("pseudo").toString();
            ServletContext app = getServletContext();
            GestionGroupes groupes = (GestionGroupes) app.getAttribute("groupes");
            groupes.getGroupeByNom(nomGroupe).addMembre(pseudo);
            session.setAttribute("groupe", nomGroupe);
            response.sendRedirect(request.getContextPath() + "/home");
        }
    }

}
