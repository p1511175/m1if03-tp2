/*package fr.univlyon1.m1if.m1if03.servlets.filters;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "LoginFilter", urlPatterns = "/*",
        initParams = {@WebInitParam(name = "excluded", value = "/index.html")})
public class LoginFilter implements Filter {

    private String excluded;

    @Override
    public void init(FilterConfig filterConfig) {
        this.excluded = filterConfig.getInitParameter("excluded");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String servletPath = req.getServletPath();

        HttpSession session = req.getSession(true);
        String pseudo = (String) session.getAttribute("pseudo");

        HttpServletResponse res = (HttpServletResponse) response;

        if (this.excluded.equals(servletPath)) {
            chain.doFilter(request, response);
        } else {
            if (pseudo != null) {
                chain.doFilter(request, response);
            } else {
                if (servletPath.equals("/init") && "POST".equalsIgnoreCase(req.getMethod())) {
                    req.getRequestDispatcher("init").forward(req, res);
                } else {
                    res.setStatus(302);
                    res.sendRedirect(req.getContextPath() + "/index.html");
                }
            }
        }
    }
}
*/