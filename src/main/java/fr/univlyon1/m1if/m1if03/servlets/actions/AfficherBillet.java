package fr.univlyon1.m1if.m1if03.servlets.actions;

import fr.univlyon1.m1if.m1if03.classes.billet.Billet;
import fr.univlyon1.m1if.m1if03.classes.billet.GestionBillets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AfficherBillet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("pseudo")==null) {
            response.sendRedirect(request.getContextPath() + "/init");
        }

        else {
            Billet billet = ((GestionBillets) getServletContext().getAttribute("billets"))
                    .getBilletByTitre(request.getPathInfo().substring(1));
            if (billet != null) {
                request.setAttribute("billet", billet);
                request.getRequestDispatcher("/WEB-INF/jsp/billet.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/init");
            }
        }
    }
}
