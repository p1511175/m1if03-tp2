package fr.univlyon1.m1if.m1if03.servlets.actions;

import fr.univlyon1.m1if.m1if03.classes.billet.Billet;
import fr.univlyon1.m1if.m1if03.classes.billet.GestionBillets;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SaisirBillet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String titre = request.getParameter("titre");
        String contenu = request.getParameter("contenu");

        if (titre != null && !titre.isEmpty() && contenu != null && !contenu.isEmpty()) {
            ServletContext app = getServletContext();
            GestionBillets billets = (GestionBillets) app.getAttribute("billets");

            if (billets.getBilletByTitre(titre)==null) {
                Billet billet = new Billet(
                        titre,
                        contenu,
                        request.getSession().getAttribute("pseudo").toString(),
                        request.getSession().getAttribute("groupe").toString()
                );
                billets.add(billet);
                response.sendRedirect(request.getContextPath() + "/init");
            } else {
                response.sendRedirect(request.getContextPath() + "/saisirbillet");
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/saisirbillet");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("pseudo")!=null) {
            request.getRequestDispatcher("/WEB-INF/jsp/saisirbillet.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/init");
        }
    }
}
