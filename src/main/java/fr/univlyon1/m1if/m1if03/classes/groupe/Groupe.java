package fr.univlyon1.m1if.m1if03.classes.groupe;

import java.util.ArrayList;
import java.util.List;

public class Groupe {
    private String nom, description, proprio;
    private List<String> membres;

    public Groupe(String nom, String description, String proprio) {
        this.nom = nom;
        this.description = description;
        this.proprio = proprio;
        this.membres = new ArrayList<String>();
        this.membres.add(this.proprio);
    }

    public Groupe() {
        this.nom = "Sans nom";
        this.description = "Groupe inutile";
        this.proprio = "Personne";
        this.membres = new ArrayList<String>();
        this.membres.add(this.proprio);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProprio() {
        return proprio;
    }

    public void setProprio(String proprio) {
        this.proprio = proprio;
    }

    public List<String> getMembres() {
        return membres;
    }

    public void setMembres(List<String> membres) {
        this.membres = membres;
    }


    public void addMembre(String user) {
        this.membres.add(user);
    }

    public void removeMembre(String user) {
        this.membres.remove(user);
    }

    public boolean isMembre(String user) {
        for(String u : membres) {
            if (u.equals(user)){
                return true;
            }
        }
        return false;
    }

}
