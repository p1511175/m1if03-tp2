package fr.univlyon1.m1if.m1if03.classes.billet;


import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Billet {
    private String titre, contenu, auteur, time, groupe;
    private List<Comment> commentaires;

    public Billet() {
        this.titre = "Rien";
        this.contenu = "Vide";
        this.auteur = "Personne";
        this.groupe = "Aucun";
        this.commentaires = new ArrayList<>();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        this.time = java.time.LocalTime.now().format(dtf);
    }

    public Billet(String titre, String contenu, String auteur, String groupe) {
        this.titre = titre;
        this.contenu = contenu;
        this.auteur = auteur;
        this.groupe = groupe;
        this.commentaires = new ArrayList<>();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        this.time = java.time.LocalTime.now().format(dtf);
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public List<Comment> getCommentaires() { return commentaires; }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getGroupe() {
        return groupe;
    }

    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    public void addCom(Comment com) { this.commentaires.add(com); }

    public void setComs(ArrayList<Comment> coms) { this.commentaires = coms; }

    public String getTime() { return this.time; }
}
