package fr.univlyon1.m1if.m1if03.classes.billet;

import java.time.format.DateTimeFormatter;

public class Comment {

    private String auteur, contenu, time;

    public Comment(String auteur, String contenu) {
        this.auteur = auteur;
        this.contenu = contenu;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        this.time = java.time.LocalTime.now().format(dtf);
    }

    public String getAuteur() {
        return auteur;
    }

    public String getContenu() {
        return contenu;
    }

    public String getTime() {
        return time;
    }
}
