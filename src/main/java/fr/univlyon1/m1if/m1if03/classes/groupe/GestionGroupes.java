package fr.univlyon1.m1if.m1if03.classes.groupe;

import java.util.ArrayList;

public class GestionGroupes extends ArrayList<Groupe>{

    @Override
    public boolean add(Groupe g) {
        for(Groupe grp : this) {
            if(grp.getNom().equals(g.getNom())) {
                return false;
            }
        }
        return super.add(g);
    }

    public Groupe getGroupe(int i) {
        return this.get(i);
    }

    public Groupe getLastGroupe() {
        if (this.size() > 0)
            return this.get(this.size() - 1);
        throw new IndexOutOfBoundsException("Erreur dans l'appel à la fonction getLastBillet");
    }

    public Groupe getGroupeByNom(String titre) {
        for (Groupe g : this) {
            if (g.getNom().equals(titre)) {
                return g;
            }
        }
        return null;
    }
}
