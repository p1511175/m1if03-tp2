package fr.univlyon1.m1if.m1if03.classes.billet;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class GestionBillets extends ArrayList<Billet>{

    private LocalDateTime lastModified = LocalDateTime.now();

    @Override
    public boolean add(Billet billet) {
        this.lastModified = LocalDateTime.now();
        return super.add(billet);
    }

    public Billet getBillet(int i) { return this.get(i); }

    public LocalDateTime getLastModified() {
        return this.lastModified;
    }

    public Billet getBilletByTitre(String titre) {
        for (Billet b : this) {
            if (b.getTitre().equals(titre)) {
                return b;
            }
        }
        return null;
    }

    public Billet getLastBillet() {
        if (this.size() > 0)
            return this.get(this.size() - 1);
        throw new IndexOutOfBoundsException("Erreur dans l'appel à la fonction getLastBillet");
    }
}
